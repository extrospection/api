{
  description = "Let some website figure out how you really feel";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs";
    poetry2nix.url = "github:nix-community/poetry2nix";
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix }:
    {
      overlay = nixpkgs.lib.composeManyExtensions [
        poetry2nix.overlay
        (final: prev: let
          extrospection-api-override = {
            packageOverrides = self: super: {
              extrospection-api = prev.poetry2nix.mkPoetryApplication {
                projectDir = ./.;
              };
            };
          };
        in {
          python39 = prev.python39.override extrospection-api-override;

          extrospection-api-runner = let
            python = final.python39.withPackages(pythonPackages: with pythonPackages; [ extrospection-api ]);
          in final.writeShellScriptBin "extrospection-api-runner" ''
            ${python}/bin/uvicorn --app-dir=${python}/lib/python3.9/site-packages extrospection_api:main
          '';
        })
      ];
    } // (flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ self.overlay ];
        };
      in
      {
        packages = {
          extrospection-api = pkgs.python39Packages.extrospection-api;
        };

        defaultPackage = pkgs.python39Packages.extrospection-api;

        apps = {
          extrospection-api-runner = pkgs.extrospection-api-runner;
        };

        defaultApp = pkgs.extrospection-api-runner;

        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            poetry

            (python39.withPackages(pythonPackages: with pythonPackages; [
              python-lsp-server
            ]))
          ];
        };
      }));
}

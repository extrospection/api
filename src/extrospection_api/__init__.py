__version__ = '0.1.0'

# external
from starlette.applications import Starlette
from starlette.routing import Mount

# internal
from extrospection_api import sentiment

ROUTES = [
    Mount("/sentiment", routes=sentiment.ROUTES)
]

app = Starlette(routes=ROUTES)

# standard
from typing import TYPE_CHECKING
import zlib

# local
from extrospection_lib import queue

# external
from starlette.requests import Request
from starlette.responses import StreamingResponse
from starlette.routing import Route

if TYPE_CHECKING:
    from typing import Generator, Optional


async def generate_events(
    request: "Request",
    last_event_id: "Optional[str]" = None,
) -> "Generator[bytes, None, None]":
    """
    """


async def create_analysis_job(request: "Request") -> "None":
    """Create a new analysis job and subscribe to get its update events.

    Expects the request body to have a ``text`` key.

    Args:
        request: The HTTP request.
    """
    job_id = queue.push()

    return StreamingResponse(
        generate_events(
            request,
            last_event_id=request.headers.get("last-event-id"),
        ),
        headers={
            "Content-type": "text/event-stream",
            "Cache-Control": "no-cache",
            "Connection": "keep-alive",
            "Content-Encoding": "deflate",
        },
    )


ROUTES = [
    Route("/", create_analysis_job, methods=["POST"]),
]
